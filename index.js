'use strict';

var http = require('http');
var request = require('request');
var tg = require('telegram-node-bot')('224171916:AAHOUmKmrmGGXaYxPl37xq1_705dOUl2lq4');


// tg.router.when(['login'], 'LoginController');
tg.router.when(['سلام'], 'hiController');
tg.router.when(['start'], 'start');
// tg.router.when(['menu'], 'menu');
// tg.router.when(['pmenu'], 'pmenu');
tg.router.when(['سیاسی-تاریخی'], 'getQuestion');
tg.router.when(['ادبیات'], 'getQuestion');
tg.router.when(['سینمایی'], 'getQuestion');
tg.router.when(['جغرافی'], 'getQuestion');
tg.router.when(['اطلاعات عمومی'], 'getQuestion');
tg.router.when(['همه جوره'], 'getQuestion');

tg.router.when(['politic'], 'getQuestion');
tg.router.when(['literature'], 'getQuestion');
tg.router.when(['general'], 'getQuestion');
tg.router.when(['geography'], 'getQuestion');
tg.router.when(['all'], 'getQuestion');
tg.router.when(['504_english_to_english'], 'getQuestion');
tg.router.when(['504_english_to_persian'], 'getQuestion');
tg.router.when(['cinema'], 'getQuestion');

tg.router.when(['mamad'], 'mamad');
// tg.router.when(['pipe'], 'pipe');
tg.router.otherwise('start');


var botan = require('botanio')('nNq96APdXhMUMl6dnb33p1CwfC-CiWF3');



tg.controller('OtherwiseController', ($) => {
    $.sendMessage($.message.text);
});
var constString = 'روی هر نوع سوالی که میخوای کلیک کن تا بازی شروع شه' + '\n' +
    'سیاسی-تاریخی ' + '  /politic' + '\n'
    + 'سینمایی' + '  /cinema' + '\n' + 'اطلاعات عمومی' + '  /general'
    + '\n' + 'جغرافی' + '  /geography' + '\n' + 'ادبیات' + '   /literature' + '\n' + 'همه جوره' + '   /all'
    + '\n' + 'لغات ۵۰۴، انگلیسی به انگلیسی' +'\n' + '  /504_english_to_english' +
    '\n' + 'لغات ۵۰۴، انگلیسی به فارسی' + '\n'+'   /504_english_to_persian' ;

var wrongAnswer = 'اشتباه جواب دادی گزینه‌ی درست این بود:' + '\n';
var correctAnswer = 'آفرین درست جواب دادی' + '\n\n';

tg.controller('LoginController', ($) => {
    tg.for('login', () => {
        $.sendMessage('wait...', function (data) {
            $.sendMessage(JSON.stringify(data));
        })
    })
});

tg.controller('mamad', ($) => {
    botan.track($.message, 'Start');
    botan.track($.message, 'mamad');
    console.log('$', $);
    $.sendMessage('سلام جیگر')
});

tg.controller('start', ($) => {
    var request = require('request');

    var options = {
        url: 'http://82.102.14.175:3000/bot/user',
        headers: {
            "content-type": "application/json"
        },
        method: "POST",
        body: JSON.stringify({ name: $.user.first_name,
            lastName: $.user.last_name,
            telegramUserName: $.user.username,
            telegramId: $.user.id.toString()})
    };

    function callback(error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log('creating botUser response: ',body)}
        else {
            console.log(error);
        }

    }

    request(options, callback);
    $.sendMessage(constString);

});


tg.controller('getQuestion', ($) => {
    var category = $.message.text;
    if ($.message.text.includes('/')) {
        category = $.message.text.substring(1);
    }

    var options = {
        url: 'http://82.102.14.175:3000/bot/brainduel/question?category=' + category,
        headers:{"telegram-id" : $.user.id.toString()}
    };


    function callback(error, response, body) {
        if (!error && response.statusCode == 200) {

            var question = JSON.parse(body);
            var firstOption = question.firstOption;
            var secondOption = question.secondOption;
            var thirdOption = question.thirdOption;
            var fourthOption = question.fourthOption;
            var correctOption = question.correctOption;
            var obj = {
                message: question.questionText,
                layout: [1, 2, 2]
            };

            obj[question.questionText] = function () {
                $.sendMessage(constString);
            };


            obj[firstOption] = function () {
                if (firstOption == correctOption) {
                    $.sendMessage(correctAnswer + constString)
                }
                else {
                    $.sendMessage(wrongAnswer + correctOption + '\n\n' + constString)
                }
            };

            obj[secondOption] = function () {
                if (secondOption == correctOption) {
                    $.sendMessage(correctAnswer + constString)
                }
                else {
                    $.sendMessage(wrongAnswer + correctOption + '\n\n' + constString)
                }
            };

            obj[thirdOption] = function () {
                if (thirdOption == correctOption) {
                    $.sendMessage(correctAnswer + constString)
                }
                else {
                    $.sendMessage(wrongAnswer + correctOption + '\n\n' + constString)
                }
            };

            obj[fourthOption] = function () {
                if (fourthOption == correctOption) {
                    $.sendMessage(correctAnswer + constString)
                }
                else {
                    $.sendMessage(wrongAnswer + correctOption + '\n\n' + constString)
                }
            };
            $.runMenu(obj);


        } else {
            console.log('question error', error);
            $.sendMessage('مشکلی پیش اومده، شرمنده سعی می‌کنیم درستش کنیم');
        }


    }

    request(options, callback);
});


var server = http.createServer(function (request, response) {
    response.writeHead(200, {"Content-Type": "text/html"});
    response.end("<a href='https://telegram.me/sampleAuthBot_bot?start=login'>Telegram Login</a>");
});


server.listen(process.env.PORT);

